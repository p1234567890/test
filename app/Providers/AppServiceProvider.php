<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Position;
use App\Models\Department;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Position::deleted(function ($position) {
            if (isset($position->users)) {
                foreach ($position->users as $user) {
                    $user->setAttribute('position_id', null)->save();
                }
            }
        });

        Department::deleted(function ($department) {
            $department->users_departments()->detach();
        });
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Position;
use App\Models\Department;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Render index page action
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dataProvider = new EloquentDataProvider(User::query());

        return view('user', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Render add page action
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $positions = Position::query()->get();
        $departments = Department::query()->get();

        return view('user_add', [
            'positions' => $positions,
            'departments' => $departments,
        ]);
    }

    /**
     * Render edit page action
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $user = User::query()->find($id);
        $positions = Position::query()->get();
        $departments = Department::query()->get();

        return view('user_edit', [
            'user' => $user,
            'positions' => $positions,
            'departments' => $departments,
        ]);
    }

    /**
     * Delete user action
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $hasAccess = $this->hasAccess('delete');

        if (!$hasAccess) {
            return response()->json([
                'result' => false
            ]);
        }

        $user = User::query()->find($id);
        $res = $user->delete();

        return response()->json([
            'result' => $res
        ]);
    }

    /**
     * Save user action
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $_POST;
        $res = false;
        $hasAccess = $this->hasAccess('save');

        if (!$hasAccess) {
            return response()->json([
                'result' => false
            ]);
        }

        if (isset($params['user_name'])) {
            $password = '';

            if (isset($params['user_id'])) {
                $user = User::query()->find($params['user_id']);
                $imageName = $user->photo;
                if (isset($params['change_password'])) {
                    $password = Hash::make($params['user_password']);
                }
            } else {
                $user = new User();
                $imageName = '';
                $password = Hash::make($params['user_password']);
            }

            $file = 'user_photo';

            if ($request->hasFile($file)) {
                $request->validate([
                    $file => '|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                $imageName = $request->$file->hashName();
                $request->$file->move(public_path('images'), $imageName);
            }

            $user_position_id = empty($params['user_position_id']) ? null : $params['user_position_id'];

            $user->setAttribute('name', $params['user_name'])
                ->setAttribute('email', $params['user_email'])
                ->setAttribute('position_id', $user_position_id)
                ->setAttribute('role', $params['user_role'])
                ->setAttribute('photo', $imageName);

            if ($password != '') {
               $user->setAttribute('password', Hash::make($params['user_password']));
            }

            $res = $user->save();

            $user->departments()->detach();
            $user->departments()->attach($request->departments);
        }

        return response()->json([
            'result' => $res
        ]);
    }
}

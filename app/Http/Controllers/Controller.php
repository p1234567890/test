<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Check access to action for currently authetificated user
     *
     * @param string $actionName
     * @return bool
     */
    protected function hasAccess(string $actionName): bool
    {
        $role = Auth::user()->role;

        $rules = [
            'Admin' => [
                'save' => 1,
                'delete' => 1,
            ],
            'Manager' => [
                'save' => 1,
            ],
            'User' => [],
        ];

        return isset($rules[$role][$actionName]) ? true : false;
    }
}

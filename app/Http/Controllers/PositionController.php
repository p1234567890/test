<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

/**
 * Class PositionController
 * @package App\Http\Controllers
 */
class PositionController extends Controller
{
    /**
     * Render index page action
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dataProvider = new EloquentDataProvider(Position::query());

        return view('position', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Render add page action
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('position_add');
    }

    /**
     * Render edit page action
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $position = Position::query()->find($id);

        return view('position_edit', [
            'position' => $position
        ]);
    }

    /**
     * Delete user action
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $position = Position::query()->find($id);
        $res = $position->delete();

        return response()->json([
            'result' => $res
        ]);
    }

    /**
     * Save user action
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save()
    {
        $params = $_POST;
        $res = false;

        if (isset($params['position_name'])) {
            if (isset($params['position_id'])) {
                $position = Position::query()->find($params['position_id']);
            } else {
                $position = new Position();
            }

            $res = $position->setAttribute('name', $params['position_name'])->save();
        }

        return response()->json([
            'result' => $res
        ]);
    }

}

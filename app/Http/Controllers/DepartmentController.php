<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

/**
 * Class DepartmentController
 * @package App\Http\Controllers
 */
class DepartmentController extends Controller
{
    /**
     * Render index page action
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dataProvider = new EloquentDataProvider(Department::query());

        return view('department', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Render add page action
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('department_add');
    }

    /**
     * Render edit page action
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $department = Department::query()->find($id);

        return view('department_edit', [
            'department' => $department
        ]);
    }

    /**
     * Delete user action
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $department = Department::query()->find($id);

        $res = $department->delete();

        return response()->json([
            'result' => $res
        ]);
    }

    /**
     * Save user action
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save()
    {
        $params = $_POST;
        $res = false;

        if (isset($params['department_name'])) {
            if (isset($params['department_id'])) {
                $department = Department::query()->find($params['department_id']);
            } else {
                $department = new Department();
            }

            $res = $department->setAttribute('name', $params['department_name'])->save();
        }

        return response()->json([
            'result' => $res
        ]);
    }
}

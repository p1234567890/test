<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('user');
Route::get('/users/add', [App\Http\Controllers\UserController::class, 'add'])->name('user');
Route::get('/users/{id}/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('user');
Route::post('/users/{id}/delete', [App\Http\Controllers\UserController::class, 'delete'])->name('user');
Route::post('/users/save', [App\Http\Controllers\UserController::class, 'save'])->name('user');

Route::get('/positions', [App\Http\Controllers\PositionController::class, 'index'])->name('position');
Route::get('/positions/add', [App\Http\Controllers\PositionController::class, 'add'])->name('position');
Route::get('/positions/{id}/edit', [App\Http\Controllers\PositionController::class, 'edit'])->name('position');
Route::post('/positions/{id}/delete', [App\Http\Controllers\PositionController::class, 'delete'])->name('position');
Route::post('/positions/save', [App\Http\Controllers\PositionController::class, 'save'])->name('position');

Route::get('/departments', [App\Http\Controllers\DepartmentController::class, 'index'])->name('department');
Route::get('/departments/add', [App\Http\Controllers\DepartmentController::class, 'add'])->name('department');
Route::get('/departments/{id}/edit', [App\Http\Controllers\DepartmentController::class, 'edit'])->name('department');
Route::post('/departments/{id}/delete', [App\Http\Controllers\DepartmentController::class, 'delete'])->name('department');
Route::post('/departments/save', [App\Http\Controllers\DepartmentController::class, 'save'])->name('department');

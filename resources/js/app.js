require('./bootstrap');

$('button#position_save').on('click', function(event) {
    $.ajax({
        url: '/positions/save',
        method: 'POST',
        data: $('form').serialize(),
        dataType: 'json',
        success: function(response) {
            success(response, 'Saved', 'positions');
        },
        error: function(response){
            toastr.error('Something wrong', 'Error');
        }
    });
});

$('a[data-position-delete="1"]').on('click', function(event) {
    event.preventDefault();
    const id = $(this).attr('href');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/positions/' + id + '/delete',
        method: 'POST',
        data: {},
        dataType: 'json',
        success: function(response) {
            success(response, 'Deleted', 'positions');
        },
        error: function(response){
            toastr.error('Something wrong', 'Error');
        }
    });
});

$('button#department_save').on('click', function(event) {
    $.ajax({
        url: '/departments/save',
        method: 'POST',
        data: $('form').serialize(),
        dataType: 'json',
        success: function(response) {
            success(response, 'Saved', 'departments');
        },
        error: function(response){
            toastr.error('Something wrong', 'Error');
        }
    });
});

$('a[data-department-delete="1"]').on('click', function(event) {
    event.preventDefault();
    const id = $(this).attr('href');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/departments/' + id + '/delete',
        method: 'POST',
        data: {},
        dataType: 'json',
        success: function(response) {
            success(response, 'Deleted', 'departments');
        },
        error: function(response){
            toastr.error('Something wrong', 'Error');
        }
    });
});

$('button#user_save').on('click', function(event) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var data = new FormData();

    var form_data = $('#user-form').serializeArray();
    $.each(form_data, function (key, input) {
        data.append(input.name, input.value);
    });

    var file_data = $('input[name="user_photo"]')[0].files;
    for (var i = 0; i < file_data.length; i++) {
        data.append("user_photo", file_data[i]);
    }

    $.ajax({
        url: '/users/save',
        method: 'POST',
        data: data,
        processData: false,
        contentType: false,
        success: function(response) {
            success(response, 'Saved', 'users');
        },
        error: function(response){
            toastr.error('Something wrong', 'Error');
        }
    });
});

$('a[data-user-delete="1"]').on('click', function(event) {
    event.preventDefault();
    const id = $(this).attr('href');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/users/' + id + '/delete',
        method: 'POST',
        data: {},
        dataType: 'json',
        success: function(response) {
            success(response, 'Deleted', 'users');
        },
        error: function(response){
            toastr.error('Something wrong', 'Error');
        }
    });
});

function success(response, action, href) {
    if (response.result && response.result === true) {
        toastr.success(action, 'Success');
        window.location.href = '/' + href;
    } else {
        toastr.error('Access denied', 'Attention');
    }
}

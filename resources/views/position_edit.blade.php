@extends('layouts.app')

@section('content')
    <div class="container-sm">
        <h5>Edit position</h5>
        <form>
            <div class="mb-3">
                <input type="hidden" class="form-control" id="position_id" name="position_id" value="{{$position->id}}">
            </div>
            <div class="mb-3">
                <label for="position_name" class="form-label">Name</label>
                <input type="text" class="form-control" id="position_name" name="position_name" value="{{$position->name}}">
            </div>
        </form>
        <button class="btn btn-primary" id="position_save">Save</button>
    </div>
@endsection

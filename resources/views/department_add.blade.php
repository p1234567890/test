@extends('layouts.app')

@section('content')
    <div class="container-sm">
        <h5>Add new department</h5>
        <form>
            <div class="mb-3">
                <label for="department_name" class="form-label">Name</label>
                <input type="text" class="form-control" id="department_name" name="department_name">
            </div>
        </form>
        <button class="btn btn-primary" id="department_save">Save</button>
    </div>
@endsection



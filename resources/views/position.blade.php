@extends('layouts.app')

<style>
    .btn-add {
     position: absolute;
     z-index: 1000;
     margin-top: 50px;
     margin-left: 20px;
    }
</style>

@section('content')
    <div>
        <a class="btn btn-primary btn-add" href="{{ url('/positions/add') }}" role="button">Add</a>
    </div>
    @php
        $gridData = [
                'dataProvider' => $dataProvider,
                'title' => 'Positions',
                'url' => '/positions/add',
                'useFilters' => false,
                'columnFields' => [
                    'id',
                    'name',
                    [
                        'attribute' => 'created_at',
                        'value' => function ($data) {
                            return $data->created_at->format('d.m.Y H:i:s');
                        },
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function ($data) {
                            return $data->updated_at->format('d.m.Y H:i:s');
                        },
                    ],
                    [
                        'label' => 'Actions',
                        'class' => Itstructure\GridView\Columns\ActionColumn::class,
                        'actionTypes' => [
                            'edit' => function ($data) {
                                return '/positions/' . $data->id . '/edit';
                            },
                            [
                                'class' => Itstructure\GridView\Actions\Delete::class,
                                'url' => function ($data) {
                                    return $data->id;
                                },
                                'htmlAttributes' => [
                                    'style' => 'color: yellow; font-size: 16px;',
                                    'onclick' => 'return window.confirm("Are you sure you want to delete?");',
                                    'data-position-delete' => '1'
                                ]
                            ]
                        ],
                        'htmlAttributes' => [
                            'width' => '10%'
                        ],
                    ]
                ]
            ];
    @endphp
    @gridView($gridData)
@endsection

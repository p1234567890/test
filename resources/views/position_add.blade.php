@extends('layouts.app')

@section('content')
    <div class="container-sm">
        <h5>Add new position</h5>
        <form>
            <div class="mb-3">
                <label for="position_name" class="form-label">Name</label>
                <input type="text" class="form-control" id="position_name" name="position_name">
            </div>
        </form>
        <button class="btn btn-primary" id="position_save">Save</button>
    </div>
@endsection

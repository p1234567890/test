@extends('layouts.app')

<style>
    .btn-add {
        position: absolute;
        z-index: 1000;
        margin-top: 50px;
        margin-left: 20px;
    }
</style>

@section('content')
    @php
        $gridData = [
            'dataProvider' => $dataProvider,
            'title' => 'Users',
            'useFilters' => false,
            'columnFields' => [
                'id',
                'name',
                'email',
                [
                    'attribute' => 'position',
                    'label' => 'Position',
                    'filter' => false,
                    'htmlAttributes' => [
                        'width' => '15%'
                    ],
                    'value' => function ($data) {
                        return isset($data->position) ? $data->position->name : '';
                    },
                ],
                [
                'label' => 'Photo',
                    'value' => function ($data) {
                        if (!empty($data->photo)) {
                            return DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $data->photo;
                        }  else {
                            return DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'noimage.png';
                        }
                    },
                    'filter' => false,
                    'format' => [ // Set special formatter. If $row->icon value is a url to image, it will be inserted in to 'src' attribute of <img> tag.
                        'class' => Itstructure\GridView\Formatters\ImageFormatter::class,
                        'htmlAttributes' => [
                            'width' => '100'
                        ]
                    ]
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function ($data) {
                        return $data->created_at->format('d.m.Y H:i:s');
                    },
                ],
                [
                    'attribute' => 'updated_at',
                    'value' => function ($data) {
                        return $data->updated_at->format('d.m.Y H:i:s');
                    },
                ],
                [
                    'label' => 'Actions',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit' => function ($data) {
                            return '/users/' . $data->id . '/edit';
                        },
                        [
                            'class' => Itstructure\GridView\Actions\Delete::class,
                            'url' => function ($data) {
                                return $data->id;
                            },
                            'htmlAttributes' => [
                                'style' => 'color: yellow; font-size: 16px;',
                                'onclick' => 'return window.confirm("Are you sure you want to delete?");',
                                'data-user-delete' => '1'
                            ]
                        ]
                    ],
                    'htmlAttributes' => [
                        'width' => '10%'
                    ],
                ]
            ]
        ];
    @endphp

    <div>
        <a class="btn btn-primary btn-add" href="{{ url('/users/add') }}" role="button">Add</a>
    </div>

    @gridView($gridData)
@endsection

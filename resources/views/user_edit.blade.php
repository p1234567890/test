@extends('layouts.app')

@section('content')
    <div class="container-sm">
        <h5>Edit user</h5>
        <form id="user-form" enctype="multipart/form-data">
            <div class="mb-3">
                <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{$user->id}}">
            </div>
            <div class="mb-3">
                <label for="user_name" class="form-label">Name</label>
                <input type="text" class="form-control" id="user_name" name="user_name" value="{{$user->name}}">
            </div>
            <div class="mb-3">
                <label for="user_email" class="form-label">Email</label>
                <input type="text" class="form-control" id="user_email" name="user_email" value="{{$user->email}}">
            </div>
            <div class="mb-3">
                <label for="user_password" class="form-label">Password</label>
                <input type="password" class="form-control" id="user_password" name="user_password" value="{{$user->password}}">
            </div>
            <div class="mb-3">
                <label for="user_password" class="form-label">Change password</label>
                <input type="checkbox" id="change_password" name="change_password">
            </div>
            <div class="mb-3">
                <label class="form-label">Departments</label>
                @php
                    foreach($departments as $department) {
                @endphp
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="{{$department->id}}" name="departments[]"
                           id="department_{{$department->id}}" @php echo $user->isChecked($department->id); @endphp>
                    <label class="form-check-label" for="department_{{$department->id}}">
                        {{$department->name}}
                    </label>
                </div>
                @php
                    }
                @endphp
            </div>
            <div class="mb-3">
                <label for="user_position_id" class="form-label">Position</label>
                <select class="form-control" id="user_position_id" name="user_position_id">
                    <option value="" @php if (!isset($user->position_id)) echo 'selected' @endphp>---</option>
                    @php
                        foreach($positions as $position) {
                    @endphp
                            <option value="{{$position->id}}" @php if ($user->position_id == $position->id) echo 'selected' @endphp>{{$position->name}}</option>
                    @php
                        }
                    @endphp
                </select>
            </div>
            <div class="mb-3">
                <label for="user_role" class="form-label">Role</label>
                <select class="form-control" id="user_role" name="user_role">
                    <option value="Admin" @php if ($user->role == 'Admin') echo 'selected' @endphp>Admin</option>
                    <option value="Manager" @php if ($user->role == 'Manager') echo 'selected' @endphp>Manager</option>
                    <option value="User" @php if ($user->role == 'User') echo 'selected' @endphp>User</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="user_photo" class="form-label">Photo</label>
                <input type="file" class="form-control" id="user_photo" name="user_photo">
            </div>
        </form>
        <button class="btn btn-primary" id="user_save">Save</button>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container-sm">
        <h5>Add user</h5>
        <form id="user-form" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="user_name" class="form-label">Name</label>
                <input type="text" class="form-control" id="user_name" name="user_name" value="">
            </div>
            <div class="mb-3">
                <label for="user_email" class="form-label">Email</label>
                <input type="text" class="form-control" id="user_email" name="user_email" value="">
            </div>
            <div class="mb-3">
                <label for="user_password" class="form-label">Password</label>
                <input type="password" class="form-control" id="user_password" name="user_password" value="">
            </div>
            <div class="mb-3">
                <label class="form-label">Departments</label>
                @php
                    foreach($departments as $department) {
                @endphp
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="{{$department->id}}" name="departments[]"
                           id="department_{{$department->id}}" >
                    <label class="form-check-label" for="department_{{$department->id}}">
                        {{$department->name}}
                    </label>
                </div>
                @php
                    }
                @endphp
            </div>
            <div class="mb-3">
                <label for="user_position_id" class="form-label">Position</label>
                <select class="form-control" id="user_position_id" name="user_position_id">
                    @php
                        foreach($positions as $position) {
                    @endphp
                    <option value="{{$position->id}}">{{$position->name}}</option>
                    @php
                        }
                    @endphp
                </select>
            </div>
            <div class="mb-3">
                <label for="user_role" class="form-label">Role</label>
                <select class="form-control" id="user_role" name="user_role">
                    <option value="Admin">Admin</option>
                    <option value="Manager">Manager</option>
                    <option value="User">User</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="user_photo" class="form-label">Photo</label>
                <input type="file" class="form-control" id="user_photo" name="user_photo">
            </div>
        </form>
        <button class="btn btn-primary" id="user_save">Save</button>
    </div>
@endsection

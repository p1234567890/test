@extends('layouts.app')

@section('content')
    <div class="container-sm">
        <h5>Edit department</h5>
        <form>
            <div class="mb-3">
                <input type="hidden" class="form-control" id="department_id" name="department_id" value="{{$department->id}}">
            </div>
            <div class="mb-3">
                <label for="department_name" class="form-label">Name</label>
                <input type="text" class="form-control" id="department_name" name="department_name" value="{{$department->name}}">
            </div>
        </form>
        <button class="btn btn-primary" id="department_save">Save</button>
    </div>
@endsection



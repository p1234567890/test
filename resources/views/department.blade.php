@extends('layouts.app')

<style>
    .btn-add {
        position: absolute;
        z-index: 1000;
        margin-top: 50px;
        margin-left: 20px;
    }
</style>

@section('content')
    @php
        $gridData = [
            'dataProvider' => $dataProvider,
            'title' => 'Departments',
            'useFilters' => false,
            'columnFields' => [
                'id',
                'name',
                [
                    'attribute' => 'created_at',
                    'value' => function ($data) {
                        return $data->created_at->format('d.m.Y H:i:s');
                    },
                ],
                [
                    'attribute' => 'updated_at',
                    'value' => function ($data) {
                        return $data->updated_at->format('d.m.Y H:i:s');
                    },
                ],
                [
                    'label' => 'Actions', // Optional
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [ // Required
                        'edit' => function ($data) {
                            return '/departments/' . $data->id . '/edit';
                        },
                        [
                            'class' => Itstructure\GridView\Actions\Delete::class,
                            'url' => function ($data) {
                                return $data->id;
                            },
                            'htmlAttributes' => [
                                'style' => 'color: yellow; font-size: 16px;',
                                'onclick' => 'return window.confirm("Are you sure you want to delete?");',
                                'data-department-delete' => '1'
                            ]
                        ]
                    ],
                    'htmlAttributes' => [
                        'width' => '10%',
                    ]
                ]
            ]
        ];
    @endphp

    <div>
        <a class="btn btn-primary btn-add" href="{{ url('/departments/add') }}" role="button">Add</a>
    </div>

    @gridView($gridData)
@endsection
